(ns mrcop.routes.webhook-test
  (:require [clojure.test :refer :all]
            [ring.mock.request :refer :all]
            [mrcop.handler :refer :all]))

(deftest post-webhook-header-test
  (testing "POST /webhook with valid headers"
    (let [body (slurp "samples/merge-request-webhook.json")
          request-with-gitlab-header (header (request :post "/webhook" body) "X-Gitlab-Event" "Merge Request Hook")
          response ((app) (content-type request-with-gitlab-header "application/json"))]
      (is (= 200 (:status response)))))

  (testing "POST /webhook with invalid headers"
    (let [response ((app) (header (request :post "/webhook") "X-Event" "Merge Request Hook"))]
      (is (= 412 (:status response)))
      (is (= "Invalid header X-Gitlab-Event" (:body response))))))

(deftest post-webhook-get-branch-name-test
  (testing "POST /webhook with invalid project's contribute.json"
    (let [body (slurp "samples/merge-request-webhook-invalid.json")
          request-with-gitlab-header (header (request :post "/webhook" body) "X-Gitlab-Event" "Merge Request Hook")
          response ((app) (content-type request-with-gitlab-header "application/json"))
          expected-errors (str "Current parameter for subject_must_be_longer_than is invalid, "
                               "Current parameter for subject_must_start_with_case is invalid, "
                               "Current parameter for subject_cannot_be_empty is invalid")]
      (is (= 412 (:status response)))
      (is (= expected-errors (:body response)))))
  
  (testing "POST /webhook with valid body"
    (let [body (slurp "samples/merge-request-webhook.json")
          request-with-gitlab-header (header (request :post "/webhook" body) "X-Gitlab-Event" "Merge Request Hook")
          response ((app) (content-type request-with-gitlab-header "application/json"))]
      (is (= 200 (:status response)))
      (is (= "ok" (:body response))))))
