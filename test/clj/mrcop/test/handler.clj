(ns mrcop.test.handler
  (:require [clojure.test :refer :all]
            [ring.mock.request :refer :all]
            [mrcop.handler :refer :all]))

(deftest test-app
  (testing "main route"
    (let [response ((app) (request :get "/"))]
      (is (= 200 (:status response)))))

  (testing "webhook routes"
    (let [response ((app) (request :post "/webhook"))]
      (is (= 200 (:status response)))
      (is (= "Get .contribute.json from: /raw//.contribute.json" (:body response)))))

  (testing "not-found route"
    (let [response ((app) (request :get "/invalid"))]
      (is (= 404 (:status response))))))
