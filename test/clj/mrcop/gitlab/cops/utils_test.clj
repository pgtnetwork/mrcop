(ns mrcop.gitlab.cops.utils-test
  (:require [clojure.test :refer :all]
            [mrcop.gitlab.cops.utils :refer :all]))

(deftest test-is-empty?
  (testing "returns true when has an empty string"
    (is (= true (is-empty? ""))))
  (testing "returns false when has a string with value"
    (is (= false (is-empty? "value")))))

(deftest test-longer-than
  (testing "returns true when is longer than 3"
    (is (= true (longer-than 3 "four"))))
  (testing "returns false when is not longer than 3"
    (is (= false (longer-than 3 "two")))))

(deftest test-shorter-than
  (testing "returns true when is shorter than 3"
    (is (= true (shorter-than 3 "ab"))))
  (testing "returns false when is not shorter than 3"
    (is (= false (shorter-than 3 "abc")))))

(deftest test-start-with-case
  (testing "returns true when starts with uppercase"
    (is (= true (start-with-case "upper" "Abc"))))

  (testing "returns false when does not starts with uppercase"
    (is (= false (start-with-case "upper" "abc"))))

  (testing "returns true when starts with lower"
    (is (= true (start-with-case "lower" "abc"))))

  (testing "returns false when does not starts with lower"
    (is (= false (start-with-case "lower" "Abc"))))

  (testing "returns false when starts with a number"
    (is (= false (start-with-case "upper" "1abc"))))

  (testing "returns false when starts with a special character"
    (is (= false (start-with-case "upper" "%asdf"))))

  (testing "raises exception when receives an invalid option"
    (is
     (thrown-with-msg?
      IllegalArgumentException #"Invalid case 'wrong'. Use lower or upper instead."
      (start-with-case "wrong" "abc")))))

(deftest test-ends-with-dot?
  (testing "returns true when ends with a dot"
    (is (= true (ends-with-dot? "Abc."))))
  (testing "returns false when does not end with a dot"
    (is (= false (ends-with-dot? "Abc")))))

(deftest test-includes-prefix
  (testing "returns true when have at least one of the prefixes"
    (is (= true (includes-prefix '("bug" "asdf") "bug: my subject"))))
  (testing "returns false when does not have one of the prefixes"
    (is (= false (includes-prefix '("shit" "bag") "bug: my subject"))))
  (testing "returns false when is not in the beginning"
    (is (= false (includes-prefix '("my") "bug: my subject")))))

(deftest test-contains-issue?
  (testing "returns true when contains an issue"
    (is (= true (contains-issue? "Test #19"))))
  (testing "returns false when does not contain an issue"
    (is (= false (contains-issue? "Test issue 19")))))

(deftest test-explicity-does-not-contain-issue
  (testing "returns true when contains 'no issue'"
    (is (= true (explicity-does-not-contain-issue "Fix no issue"))))
  (testing "returns true when contains 'any issue'"
    (is (= true (explicity-does-not-contain-issue "This fix does not closes any issues"))))
  (testing "returns false when does not contain 'no issue' or 'any issue'"
    (is (= false (explicity-does-not-contain-issue "Whatever"))))
  )

(deftest test-contains-jira-issue?
  (testing "returns true when contains jira issue"
    (is (= true (contains-jira-issue? "My MR NPD-1")))
    (is (= true (contains-jira-issue? "My MR NP-12")))
    (is (= true (contains-jira-issue? "My MR NPDS-12")))
    (is (= true (contains-jira-issue? "My MR NPD-1222"))))
  (testing "returns false when does contains jira issue"
    (is (= false (contains-jira-issue? "My MR")))))

(deftest test-must-include-verification-steps
  (testing "returns true when includes a verification step"
    (is (= true (must-include-verification-steps "before to verify")))
    (is (= true (must-include-verification-steps "before TO VERIFY")))
    (is (= true (must-include-verification-steps "before verification steps")))
    (is (= true (must-include-verification-steps "before VERIFICATION STEPS")))
    (is (= true (must-include-verification-steps "before verification instructions")))
    (is (= true (must-include-verification-steps "before VERIFICATION INSTRUCTIONS")))
    (is (= true (must-include-verification-steps "before to test")))
    (is (= true (must-include-verification-steps "before TO TEST")))
    (is (= true (must-include-verification-steps "before testing steps")))
    (is (= true (must-include-verification-steps "before TESTING STEPS")))
    (is (= true (must-include-verification-steps "before testing instructions")))
    (is (= true (must-include-verification-steps "before TESTING INSTRUCTIONS")))
    (is (= true (must-include-verification-steps "before to review")))
    (is (= true (must-include-verification-steps "before TO REVIEW")))
    (is (= true (must-include-verification-steps "before review steps")))
    (is (= true (must-include-verification-steps "before REVIEW STEPS")))
    (is (= true (must-include-verification-steps "before review instructions")))
    (is (= true (must-include-verification-steps "before REVIEW INSTRUCTIONS"))))
  (testing "returns false when does not include a verification step"
    (is (= false (must-include-verification-steps "no steps here")))))
