(ns mrcop.gitlab.contribute-validator-test
  (:require [clojure.test :refer :all]
            [clojure.data.json :as json]
            [mrcop.gitlab.contribute-validator :refer :all]))

(deftest test-contribute-validator
  (testing "with valid data"
    (let [parsed-contribute (json/read-str (slurp "samples/contribute.json"))]
      (is (= {:valid true, :errors '()} (validate-contribute parsed-contribute)))))

  (testing "with invalid body_must_include_screenshot content"
    (let [raw-contribute {"merge_request" {"body_must_include_screenshot" true}}
          parsed-contribute raw-contribute]
      (is (=
           {:valid false,:errors '("Current parameter for body_must_include_screenshot is invalid")}
           (validate-contribute parsed-contribute)))))

  (testing "with invalid body_must_close_issue content"
    (let [raw-contribute {"merge_request" {"body_must_close_issue" 42}}
          parsed-contribute raw-contribute]
      (is (=
           {:valid false,:errors '("Current parameter for body_must_close_issue is invalid")}
           (validate-contribute parsed-contribute)))))

  (testing "with invalid subject_must_not_end_with_dot content"
    (let [raw-contribute {"merge_request" {"subject_must_not_end_with_dot" 42}}
          parsed-contribute raw-contribute]
      (is (=
           {:valid false,:errors '("Current parameter for subject_must_not_end_with_dot is invalid")}
           (validate-contribute parsed-contribute)))))

  (testing "with invalid body_must_include_jira_issue content"
    (let [raw-contribute {"merge_request" {"body_must_include_jira_issue" 42}}
          parsed-contribute raw-contribute]
      (is (=
           {:valid false,:errors '("Current parameter for body_must_include_jira_issue is invalid")}
           (validate-contribute parsed-contribute)))))

  (testing "with invalid body_must_include_issue content"
    (let [raw-contribute {"merge_request" {"body_must_include_issue" 42}}
          parsed-contribute raw-contribute]
      (is (=
           {:valid false,:errors '("Current parameter for body_must_include_issue is invalid")}
           (validate-contribute parsed-contribute)))))

  (testing "with invalid subject_cannot_be_empty content"
    (let [raw-contribute {"merge_request" {"subject_cannot_be_empty" 42}}
          parsed-contribute raw-contribute]
      (is (=
           {:valid false,:errors '("Current parameter for subject_cannot_be_empty is invalid")}
           (validate-contribute parsed-contribute)))))

  (testing "with invalid subject_must_include_issue content"
    (let [raw-contribute {"merge_request" {"subject_must_include_issue" 42}}
          parsed-contribute raw-contribute]
      (is (=
           {:valid false,:errors '("Current parameter for subject_must_include_issue is invalid")}
           (validate-contribute parsed-contribute)))))

  (testing "with invalid subject_must_include_prefix content"
    (let [raw-contribute {"merge_request" {"subject_must_include_prefix" 42}}
          parsed-contribute raw-contribute]
      (is (=
           {:valid false,:errors '("Current parameter for subject_must_include_prefix is invalid")}
           (validate-contribute parsed-contribute)))))

  (testing "with invalid subject_must_start_with_case content"
    (let [raw-contribute {"merge_request" {"subject_must_start_with_case" 42}}
          parsed-contribute raw-contribute]
      (is (=
           {:valid false,:errors '("Current parameter for subject_must_start_with_case is invalid")}
           (validate-contribute parsed-contribute)))))

  (testing "with invalid body_must_include_verification_steps content"
    (let [raw-contribute {"merge_request" {"body_must_include_verification_steps" 42}}
          parsed-contribute raw-contribute]
      (is (=
           {:valid false,:errors '("Current parameter for body_must_include_verification_steps is invalid")}
           (validate-contribute parsed-contribute)))))

  (testing "with invalid subject_must_be_longer_than content"
    (let [raw-contribute {"merge_request" {"subject_must_be_longer_than" "whatever"}}
          parsed-contribute raw-contribute]
      (is (=
           {:valid false,:errors '("Current parameter for subject_must_be_longer_than is invalid")}
           (validate-contribute parsed-contribute)))))

  (testing "with invalid subject_must_include_jira_issue content"
    (let [raw-contribute {"merge_request" {"subject_must_include_jira_issue" 42}}
          parsed-contribute raw-contribute]
      (is (=
           {:valid false,:errors '("Current parameter for subject_must_include_jira_issue is invalid")}
           (validate-contribute parsed-contribute)))))

  (testing "with invalid body_cannot_be_empty content"
    (let [raw-contribute {"merge_request" {"body_cannot_be_empty" 42}}
          parsed-contribute raw-contribute]
      (is (=
           {:valid false,:errors '("Current parameter for body_cannot_be_empty is invalid")}
           (validate-contribute parsed-contribute)))))

  (testing "with invalid subject_must_be_shorter_than content"
    (let [raw-contribute {"merge_request" {"subject_must_be_shorter_than" "whatever"}}
          parsed-contribute raw-contribute]
      (is (=
           {:valid false,:errors '("Current parameter for subject_must_be_shorter_than is invalid")}
           (validate-contribute parsed-contribute)))))

  (testing "with invalid subject_must_end_with_dot content"
    (let [raw-contribute {"merge_request" {"subject_must_end_with_dot" 42}}
          parsed-contribute raw-contribute]
      (is (=
           {:valid false,:errors '("Current parameter for subject_must_end_with_dot is invalid")}
           (validate-contribute parsed-contribute)))))

  (testing "with invalid more than one errors"
    (let [raw-contribute {"merge_request" {"body_must_include_issue" 42
                                           "subject_must_end_with_dot" 42}}
          parsed-contribute raw-contribute]
      (is (=
           {:valid false,:errors '("Current parameter for subject_must_end_with_dot is invalid"
                                   "Current parameter for body_must_include_issue is invalid")}
           (validate-contribute parsed-contribute))))))

