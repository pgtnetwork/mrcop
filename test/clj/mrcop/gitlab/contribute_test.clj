(ns mrcop.gitlab.contribute-test
  (:require [clojure.test :refer :all]
            [clojure.data.json :as json]
            [mrcop.gitlab.contribute :refer :all]))

(deftest test-contribute
  (testing "get-contribute-json"
    (is (=
         (json/read-str (slurp "samples/contribute.json"))
         (get-contribute-json "1654")))))
