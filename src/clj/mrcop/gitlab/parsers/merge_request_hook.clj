(ns mrcop.gitlab.parsers.merge-request-hook)

(defn- attributes [params]
  (:object_attributes (:params params)))

(defn- web-url [params]
  (:web_url (:source (attributes params))))

(defn- source-branch [params]
  (:source_branch (attributes params)))

(defn contribute-json-url [params]
  (str (web-url params) "/raw/" (source-branch params) "/.contribute.json"))
