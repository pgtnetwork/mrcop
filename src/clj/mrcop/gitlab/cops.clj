(ns mrcop.gitlab.cops
  (:require [mrcop.gitlab.cops.utils :as utils]))

(defn subject-cannot-be-empty [subject]
  (if (utils/is-empty? subject)
    "The subject cannot be empty."))

(defn subject-must-be-longer-than)
(defn subject-must-be-shorter-than)
(defn subject-must-start-with-case)
(defn subject-must-end-with-dot)
(defn subject-must-not-end-with-dot)
(defn subject-must-include-prefix)
(defn subject-must-include-issue)
(defn subject-must-include-jira-issue)
(defn body-cannot-be-empty)
(defn body-must-include-verification-steps)
(defn body-must-include-screenshot)
(defn body-must-close-issue)
(defn body-must-include-issue)
(defn body-must-include-jira-issue)
