(ns mrcop.gitlab.contribute
  (:require [clojure.data.json :as json]
            [clj-http.client :as http]
            [mrcop.config :as env]))

(def gitlab-api-url "https://code.locaweb.com.br/api/v3")
;; (def source-project-id "115")
(def contribute-json-file "contribute%2Ejson")
(def private_token (str "private_token=" (env/env :gitlab-private-access-token)))

(defn- build-url-for [source-project-id]
  (str
   gitlab-api-url
   "/projects/"
   source-project-id
   "/repository/files/"
   contribute-json-file
   "/raw?"
   "ref=master&"
   private_token))

(defn get-contribute-json [source-project-id]
  (json/read-str (:body (http/get (build-url-for source-project-id)))))
