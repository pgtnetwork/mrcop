(ns mrcop.gitlab.contribute-validator)

(defn- merge-request-cops [contribute]
  (keys (get contribute "merge_request")))

;; ["html", "css"]
(defn- string-list? [list] (and (vector? list) (every? string? list)))
(defn- boolean? [value] (or (true? value) (false? value)))
(defn- must-include-one-of? [list value] (.contains list value))

;; TODO: Find a better name for this shit
(def valid-merge-request-cops {"body_must_include_screenshot" string-list?,
                               "body_must_close_issue" boolean?,
                               "subject_must_not_end_with_dot" boolean?,
                               "body_must_include_jira_issue" boolean?,
                               "body_must_include_issue" boolean?,
                               "subject_cannot_be_empty" boolean?,
                               "subject_must_include_issue" boolean?,
                               "subject_must_include_prefix" string-list?,
                               "subject_must_start_with_case" #(must-include-one-of? '("upper" "lower") %),
                               "body_must_include_verification_steps" boolean?,
                               "subject_must_be_longer_than" number?,
                               "subject_must_include_jira_issue" boolean?,
                               "body_cannot_be_empty" boolean?,
                               "subject_must_be_shorter_than" number?,
                               "subject_must_end_with_dot" boolean?})

;; TODO: Mover para namespace de merge-requests
(defn- contribute-json-errors [current-contribute]
  (reduce #(let [key %2 ;; eg.: body_must_include_issue
                 errors %1
                 current-merge-request-contribute (get current-contribute "merge_request")
                 valid-key? (contains? valid-merge-request-cops key)
                 validator (get valid-merge-request-cops key)]
             (if valid-key? ;; protection if current-contribute json contains an invalid entry
               (if (validator (get current-merge-request-contribute key))
                 errors
                 (cons (str "Current parameter for "key " is invalid") errors))
               (cons (str key " is invalid") errors)))
          '() ;; %1
          (merge-request-cops current-contribute))) ;;%2

(defn validate-contribute [contribute]
  (let [errors (contribute-json-errors contribute)]
    {:valid (empty? errors), :errors errors}))
