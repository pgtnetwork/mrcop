(ns mrcop.gitlab.cops.utils)

(defn is-empty? [subject]
  (clojure.string/blank? subject))

(defn longer-than [size, subject]
  (> (count subject) size))

(defn shorter-than [size, subject]
  (< (count subject) size))

(defn start-with-case [case, subject]
  (cond
    (= case "upper") (Character/isUpperCase (get subject 0))
    (= case "lower") (Character/isLowerCase (get subject 0))
    :else (throw (IllegalArgumentException.
                  (str "Invalid case '" case "'. Use lower or upper instead.")))))

(defn ends-with-dot? [subject]
  (clojure.string/ends-with? subject "."))

(defn includes-prefix [prefixes subject]
  (true?
   (some #(clojure.string/starts-with? subject %) prefixes)))

(defn contains-issue? [text]
  (some? (re-find #"#\d+" text)))

(defn explicity-does-not-contain-issue [text]
  (true?
   (some #(clojure.string/includes? text %) '("no issue" "any issue"))))

(defn contains-jira-issue? [text]
  (some? (re-find #"[A-Z][A-Z]+\-[0-9]+" text)))

(def verification-steps-prefixes
  [#"(?i)to verify"
   #"(?i)verification steps"
   #"(?i)verification instructions"
   #"(?i)to test"
   #"(?i)testing steps"
   #"(?i)testing instructions"
   #"(?i)to review"
   #"(?i)review steps"
   #"(?i)review instructions"])

(defn must-include-verification-steps [body]
  (some?
   (some #(re-find % body) verification-steps-prefixes)))
