(ns mrcop.handler
  (:require [compojure.core :refer [routes wrap-routes]]
            [mrcop.layout :refer [error-page]]
            [mrcop.routes.home :refer [home-routes]]
            [mrcop.routes.webhook :refer [webhook-routes]]
            [compojure.route :as route]
            [mrcop.env :refer [defaults]]
            [mount.core :as mount]
            [mrcop.middleware :as middleware]))

(mount/defstate init-app
                :start ((or (:init defaults) identity))
                :stop  ((or (:stop defaults) identity)))

(def app-routes
  (routes
    (-> #'home-routes
        (wrap-routes middleware/wrap-csrf)
        (wrap-routes middleware/wrap-formats))
    (-> #'webhook-routes
        (wrap-routes middleware/wrap-formats))
    (route/not-found
      (:body
        (error-page {:status 404
                     :title "page not found"})))))


(defn app [] (middleware/wrap-base #'app-routes))
