(ns mrcop.routes.webhook
  (:require [mrcop.layout :as layout]
            [compojure.core :refer [defroutes POST]]
            [mrcop.gitlab.parsers.merge-request-hook :as mr]
            [ring.util.http-response :as response]
            [clojure.java.io :as io]
            [clojure.data.json :as json]
            [clj-http.client :as http]
            [mrcop.gitlab.contribute-validator :as contribute-validator]))

(defn webhook-page [req]
  ;; Todo
  ;;
  ;; 1. Verificar no header pelo tipo de hook que recebemos (X-Gitlab-Event: Merge Request Hook)
  (let [gitlab-header (get (:headers req) "x-gitlab-event")
        parsed-body (:body-params req)]
    (if (= "Merge Request Hook" gitlab-header)
      ;; 2. Parsear o body que recebemos e retorna um 200
      ;; (str (:body-params req))
      ;; ...
      ;; 3. Procurar pelo contribute.json do projeto da branch atual (o resto é feito assincronamente)
      (let [web-url (:web_url (:source (:object_attributes parsed-body)))
            source-branch (:source_branch (:object_attributes parsed-body))
            contribute-url (str web-url "/raw/" source-branch "/contribute.json")
            parsed-contribute-json (json/read-str (:body (http/get contribute-url)))
            ;; 4. Validar o arquivo contribute.json (em caso de erro reporta)
            contribute-validation (contribute-validator/validate-contribute parsed-contribute-json)]

        (if (:valid contribute-validation)
          ;; 5. Validar o merge request que recebemos
          ;;    5.1. Iterar sobre todos os cops validos do contribute.json e acumular os erros
          ;; 6. Postar o sucesso ou os erros no merge-request
          (str "ok")
          (response/precondition-failed (clojure.string/join ", " (:errors contribute-validation))))
        )

      (response/precondition-failed "Invalid header X-Gitlab-Event"))))

(defroutes webhook-routes
  (POST "/webhook" [] (fn [req] (webhook-page req))))
