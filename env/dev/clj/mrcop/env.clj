(ns mrcop.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [mrcop.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[mrcop started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[mrcop has shut down successfully]=-"))
   :middleware wrap-dev})
