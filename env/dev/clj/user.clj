(ns user
  (:require [mount.core :as mount]
            mrcop.core))

(defn start []
  (mount/start-without #'mrcop.core/http-server
                       #'mrcop.core/repl-server))

(defn stop []
  (mount/stop-except #'mrcop.core/http-server
                     #'mrcop.core/repl-server))

(defn restart []
  (stop)
  (start))


