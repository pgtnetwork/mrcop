(ns mrcop.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[mrcop started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[mrcop has shut down successfully]=-"))
   :middleware identity})
