FROM java:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/uberjar/mrcop.jar /mrcop/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/mrcop/app.jar"]
