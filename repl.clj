(ns whatever
  (:require [clj-http.client :as http]
            [clj-http.util :as utils]
            [clojure.data.json :as json]))

(def backend "https://code.locaweb.com.br/api/v3")
(def source-project-id "115")
(def file-path (utils/url-encode "contribute.json"))
(def private_token "private_token=cLDLbymeq2Csto86syRR")

;; /projects/:id/repository/files/:file_path/raw?private_token=asdf
(def url (str
          backend
          "/projects/"
          source-project-id
          "/repository/files/"
          file-path
          "/raw?"
          "ref=master&"
          private_token))

(def response (http/get url))
